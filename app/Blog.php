<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{

    protected $fillable =['title','description','status','thumbnail_image','category_id','file_type',''];

    /// get blogs list with status
    public static function index($status = 1,$cate_id)
    {
        return Blog::where('status',$status)->where('category_id',$cate_id)->paginate(3);
    }
}
