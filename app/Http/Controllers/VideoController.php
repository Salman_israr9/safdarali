<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Dawson\Youtube\Facades\Youtube as Youtube;
use App\Video;
class VideoController extends Controller
{

    public function checkValidation( Request $request)
    {
       $request->validate([
        'title' => 'required',
        'file' => 'required',
        'description' => 'required',
        ]);    
    }
    
    // show list of videos 
    public function index()
    {
        $videos = Video::getVideos('1');
        return view('admin.videos.list',compact('videos'));
    }
    
     // show video upload form
    public function create()
    {
        return view('admin.youtube.add');
    }

    
    public function uploadVideo($fullPathToVideo,$data,$fullpathToImage)
    {

        $video = Youtube::upload($fullPathToVideo,$data)->withThumbnail($fullpathToImage);

        return $video->getVideoId();

    }
    


    /// save video on folder uploaded /videos
    public function saveOnDrive($request,$status)
    {
    
        if($status)
        {  
            // video 
            $file = $request->video;
             $fullPath =  public_path().'/uploaded/videos/';
             
        }
        else
        {
            // thumbnail image 
            $file = $request->thumbnailImage;
            $fullPath =  public_path().'/uploaded/thumbnail/';
        }

        //Display File Extension
        $filename = time().'.'.$file->getClientOriginalExtension();
        //Move Uploaded File
        $file->move($fullPath,$filename);
        return $fullPath.$filename;
    }

    public function storeFiles($request)
    {
       
        $data = [
            'title'       =>$request->title,
            'description' => $request->description,
            'tags'	      => ['foo', 'bar', 'baz'],
            'category_id' => 10
        ];

        // uploa video and get path
        $videoPath = $this->saveOnDrive($request,1);
        // uplaod thumbnail image and get path
        $thumbnailImagePath =  $this->saveOnDrive($request,0);
        // upload video on youtube
        return $this->uploadVideo($videoPath,$data,$thumbnailImagePath);
    }

    // store or save video in database 
    public function store(Request $request)
    {

       
        //  $this->checkValidation($request);

        // save video on loacal drive and yutube channel
        $filename = $this->storeFiles($request);

        /// save in database
        try{
            // set initial video status
            $request->merge(['status'=>config('app.video_status')]);
            $request->merge(['video_id'=>$filename]);
            Video::saveVideo($request->all());
          }
          catch (\Exception $e) {
              return $e->getMessage();
          }
          
          // save alert message for success save 
        Session::flash('msg','Video is added successfully!');
        return redirect('admin/videos/list');
    }


    public function show($id)
    {
        //
    }

     public function edit($id)
    {
        $video = Video::find($id);
        return view('admin.videos.update',compact($video));
    }


    public function update(Request $request, $id)
    {
        

    }



    public function destroy($id)
    {
        //
    }
}
