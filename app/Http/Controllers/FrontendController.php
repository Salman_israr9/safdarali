<?php

namespace App\Http\Controllers;
use App\Video;
use App\Blog;

use Illuminate\Http\Request;
use App\Category;

class FrontendController extends Controller
{

    // show blogs list
    public function blogs($cat_id=0)
    {
        $categories=Category::all();
        if($cat_id==0)
          {
              $cat_id = Category::where('status',1)->first()->id;
          }

          $blogs = Blog::index(1,$cat_id);

        return view('frontend.blogs',compact('categories','blogs'));
    }

    // show blogs list
    public function blogDetail($id)
    {
        $blog_detail = Blog::find($id);
         $blogs = Blog::where('status',1)->get();

        return view('frontend.blog_detail', compact('blog_detail','blogs'));
    }

    // 498588246635-10t63e9k3e7m887mab1gmukal8qoo1nd.apps.googleusercontent.com
    // H7ux6ZO2UEqBpuKLFtI1SGIE


    // show videos list or gallery
    public function videos()
    {
         $videos = Video::getVideos('1');

        return view('frontend.videos',compact('videos'));
    }
    // show videos list or gallery
    public function contact()
    {

        return view('frontend.contact');
     }

}
