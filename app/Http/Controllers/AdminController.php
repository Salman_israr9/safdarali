<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Blog;
use App\Category;
use Illuminate\Support\Facades\Session;
class AdminController extends Controller
{

    // show account setting form set profile or update profile etc
    public function showAccountSettingForm()
    {
        return view('admin.setting.account_setting');

    }


    // show general setting form
    public function showGeneralSettingForm(){
     return view('admin.setting.general_setting');
    }

    public function blogList()
    {
        $blogs = Blog::paginate(12);
        return view('admin.blogs.list',compact('blogs'));
    }

    /// show create blog form
    public function createBlog()
    {
        $categories = Category::where('status',1)->get();
        return view('admin.blogs.create',compact('categories'));
    }

    public  function  ($url)
    {
      $url_string = parse_url($url, PHP_URL_QUERY);
      parse_str($url_string, $args);
      return isset($args['v']) ? $args['v'] : false;
    }

    // save blog in  databse
    public function saveBlog(Request $request)
    {

        // return $request;
        $filename = null;
        if($request->sendFile=='1')
        {
            if($request->has('file'))
            {
                $file  = $request->file;
                $filename = time().'.'.$file->getClientOriginalExtension();
                $file->move(public_path('blogs'),$filename);
            }
        }
        else
         $filename = $this->getYouTubeIdFromURL($request->file);

        $request->merge(['thumbnail_image'=>$filename,'file_type'=>$request->sendFile]);
        Blog::create($request->all());
        return redirect('admin/blog/list');
    }



    /// show update  blog form
    public function editBlog($id)
    {
        $blog = Blog::find($id);
        return view('admin.blogs.update',compact('blog'));
    }


    // save blog in  databse
    public function updateBlog(Request $request, $id)
    {

        Blog::find($id)->update($request->all());
        // set flash message
        Session::flash('msg','Blog is updated successfully!');
        Session::flash('alert-class','alert-success');
        return redirect('admin/blog/list');
    }


    /// show update  blog form
    public function deleteBlog($id)
    {
        Blog::find($id)->delete();

        // set flash message
        Session::flash('msg','Blog is deleted successfully!');
        Session::flash('alert-class','alert-danger');
        return redirect('admin/blog/list');
    }



    /// article route


    // show create articel form
    public function listArticle()
    {
        return view('admin.articles.list');
    }

    // show create articel form
    public function createArticle()
    {
        return view('admin.articles.create');
    }

    // show create articel form
    public function saveArticle(Request $request)
    {
        Article::create($request->all());

        // set flash message
        Session::flash('msg','Article is saved successfully!');
        Session::flash('alert-class','alert-success');
        return redirect('admin/article/list');
    }

    // show create articel form
    public function editArticle($id)
    {
        $article = Article::find($id);
        return view('admin.articles.update',compact('article'));
    }

    // show create articel form
    public function updateArticle(Request $request, $id)
    {
        Article::find($id)->update($request->all());


        // set flash message
        Session::flash('msg','Article is updated successfully!');
        Session::flash('alert-class','alert-success');
        return redirect('admin/article/list');
    }



    // category methods
    public function listCategory()
    {
        $categories = Category::paginate(10);
        return view('admin.category.list',compact('categories'));
    }
    public function createCategory()
    {
        return view('admin.category.add');
    }
    public function storeCategory(Request $request)
    {
         try {
             Category::create($request->all());
         } catch (\Throwable $th) {
             throw $th;
         }

         Session::flash('msg','Category added successfully!');
         Session::flash('alert-class','alert-success');
        return redirect('admin/category/list');
    }
    // edit category
    public function editCategory($id)
    {
        $category = Category::find($id);
        return view('admin.category.update',compact("category"));
    }

    // update  category
    public function updateCategory(Request $request,$id)
    {
         try {
             Category::find($id)->update($request->all());
         } catch (\Throwable $th) {
             throw $th;
         }
        Session::flash('msg','Category updated successfully!');
        Session::flash('alert-class','alert-success');
        return redirect('admin/category/list');
    }

    //delete blogs when delete category
    public function deleteAllBlogs($category_id)
    {
        Blog::where('category_id',$category_id)->delete();
    }

    // delete category
    public function destroyCategory($id)
    {
       // $this->deleteAllBlogs($id);
        Category::find($id)->delete();
        Session::flash('msg','Category is deleted successfully!');
        Session::flash('alert-class','alert-danger');
        return redirect('admin/category/list');
    }
}
