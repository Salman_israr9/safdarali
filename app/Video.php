<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    protected $fillable =['title','video_id','decription','status'];

    public static function saveVideo($data)
    {
        return  Video::create($data);
    }

    /// get videos with status 
    public static function getVideos($status="1")
    {
        return Video::where('status',$status)->paginate(20);
    }
}
