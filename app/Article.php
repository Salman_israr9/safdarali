<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    
    protected $fillable =['title','images','status','description'];

    
    /// get list of article with status wise 
    public static function index($status=1)
    {
        return Article::where('status',$status)->paginate(20);
    }
}
