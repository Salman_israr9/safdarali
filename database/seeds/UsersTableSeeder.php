<?php

use Illuminate\Database\Seeder;
use App\User;
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User();
        $user->name = 'safdal ali';
        $user->image = 'safdarali.jpg';
        $user->email = 'admin@gmail.com';
        $user->phone = '03022694034';
        $user->address = 'lahore ,Punjab';
        $user->email_verified_at = now();
        $user->created_at= now();
        $user->password = bcrypt('123456');
        $user->save();
    }
}
