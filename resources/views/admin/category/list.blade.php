
@extends('layouts.admin.app')
@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
  <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Category List</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Category list</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">    
        <div class="container-fluid">
        <a href="{{url('admin/category/create')}}" class="btn btn-primary btn-sm  pull-right" > Create New category</a>      <br>  <br>
        @if(Session::has('msg'))
        <p class="alert {{ Session::pull('alert-class') }}">{{ Session::pull('msg') }}</p>
        @endif
        <div class="row">
          <!--/.col (left) -->
          <!-- right column -->
              
          <table class="table table-bordered">
           <thead>
            <th>Title</th>
            <th>Status</th>
            <th>Action</th>
           </thead>
            <tbody>
            @foreach($categories as $category)
              <tr>
                <td>{{$category->title}}</td>
                @if($category->status)
                <td>Active</td>
                @else
                <td>
                  <a href="A">Passive</a>
                </td>
                @endif
                <td>  
                  <!-- edit category data  -->
                  <a href="{{url('admin/category/edit',$category->id)}}" class="btn btn-primary btn-sm">
                    <i class="fa fa-pencil"> </i>
                  </a>

                  <!-- delete btn  -->
                  <a href="{{url('admin/category/delete',$category->id)}}" 
                    click ="return confirmed('Are you sure ! you want to   delete data permanent')"  
                    class="btn btn-danger btn-sm">
                    <i class="fa fa-remove"> </i>
                  </a>

                </td>
              </tr>
            @endforeach
            </tbody>
          </table>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@stop