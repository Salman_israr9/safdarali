

@extends('../layouts/admin/app')
@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
  <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Blog Update Form</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Blog Update</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!--/.col (left) -->
          <!-- right column -->
          <div class="col-md-12">
            <!-- Horizontal Form -->
            <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title">Update New Blog</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="{{url('admin/blog/update',$blog->id)}}"  method="post">

                <!-- csrf token -->
                @csrf

                <div class="card-body">
                <!-- title enter -->
                  <div class="form-group">
                    <label for="exampleInputEmail1">Title :</label>
                    <input type="text" class="form-control" value="{{$blog->title}}" name="title" id="exampleInputEmail1" placeholder="Enter Title.." required>
                  </div>

                  <!-- file upload -->
                  <!-- detial -->

                  <div class="form-group">
                    <label for="exampleInputEmail1">Description:</label>
                    <textarea name="description" id="" placeholder="Enter description..." cols="30" rows="10" class="form-control">
                    <?php $text =$blog->description;
                     echo strip_tags($text);
                  ?></textarea>
                  </div>

                  <!-- card end -->
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary btn-sm pull-rights">Submit</button>
                </div>
              </form>
            </div>
          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@stop
