

@extends('layouts.admin.app')
@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
  <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Blog List</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Blog list</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
        <a href="{{url('admin/blog/create')}}" class="btn btn-primary btn-sm  pull-right" > Create New Blog</a>      <br>  <br>
        @if(Session::has('msg'))
        <p class="alert {{ Session::pull('alert-class') }}">{{ Session::pull('msg') }}</p>
        @endif
        <div class="row">
          <!--/.col (left) -->
          <!-- right column -->
          <table class="table table-bordered">
           <thead>
            <th>Title</th>
            <th>Status</th>
            <th>Description</th>
            <th>Action</th>
           </thead>
            <tbody>
            @foreach($blogs as $blog)
              <tr>
                <td>{{$blog->title}}</td>
                @if($blog->status)
                <td>Active</td>
                @else
                <td>
                  <a href="A">Passive</a>
                </td>
                @endif
                <td>
                <?php
                  $text =$blog->description;
                  echo strip_tags($text, '<a>');
                  ?>

                </td>
                <td>
                  <!-- edit Blog data  -->
                  <a href="{{url('admin/blog/edit',$blog->id)}}" class="btn btn-primary btn-sm">
                    <i class="fa fa-pencil"> </i>
                  </a>

                  <!-- delete btn  -->
                  <a href="{{url('admin/blog/delete',$blog->id)}}"
                    click ="return confirmed('Are you sure ! you want to   delete data permanent')"
                    class="btn btn-danger btn-sm">
                    <i class="fa fa-remove"> </i>
                  </a>

                </td>
              </tr>
            @endforeach
            </tbody>
          </table>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@stop
