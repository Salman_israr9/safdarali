
@extends('../layouts/admin/app')
@section('content')



  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
  <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Blog Create Form</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Blog Create</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!--/.col (left) -->
          <!-- right column -->
          <div class="col-md-12">
            <!-- Horizontal Form -->
            <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title">Create New Blog</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="{{url('admin/blog/save')}}"  method="post" enctype="multipart/form-data">
                
                <!-- csrf token -->
                @csrf

                <div class="card-body">
                <!-- title enter -->
                  <div class="form-group">
                    <label for="exampleInputEmail1">Title :</label>
                    <input type="text" class="form-control" name="title" id="exampleInputEmail1" placeholder="Enter Title.." required>
                  </div>
                  <!-- category -->
                  <div class="form-group">
                    <label for="exampleInputEmail1">Category :</label>
                    <select name="category_id" id="" style="height:100%" class="form-control">
                     @foreach($categories as $category)
                     <option value="{{$category->id}}">{{$category->title}}</option>
                     @endforeach
                    </select>
                  </div>

                <!-- title enter -->
                
                  <div class="form-group">
                    <label for="exampleInputEmail1">Thumbnail:
                    <strong><input type="radio" id="showImage" name="fileType" > Image</strong>
                    <strong><input type="radio" id="showVideo" name="fileType" >  Video</strong>
                    </label>
                    <input type="file" id="file" class="form-control" name="file">
                    <input type="hidden" name="sendFile" value="1" id="sendFile">
                  </div>
                  <!-- status -->
                  
                  <div class="form-group">
                    <label for="exampleInputEmail1">Status :</label>
                    <select name="status" id="" style="height:100%" class="form-control">
                      <option value="1">Active</option>
                      <option value="0">Pending</option>
                    </select>
                  </div>
                  <!-- detial -->
                  <div class="form-group" >
                    <label for="exampleInputEmail1">Description:</label>
                    <!-- <div id="summernote"><p>Hello Summernote</p></div> -->
                    <textarea name="description"  id="summernote" placeholder="Enter description..." cols="30" rows="10" class="form-control"></textarea>
                  </div>
                  <!-- card end -->
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary btn-sm pull-rights">Submit</button>
                </div>
              </form>
            </div>
          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>


  <script>
  
    $(document).ready(function() {
      // $('#summernote').summernote();

        $('#summernote').summernote({
          height: 200,                 // set editor height
          minHeight: null,             // set minimum height of editor
          maxHeight: null,             // set maximum height of editor
          focus: true                  // set focus to editable area after initializing summernote
        });

        $("#showImage").click(function(){
          $('#sendFile').val('1');
          $("#file").attr('type','file');
          $("#file").attr('placeholder','choose file');
        });

        $("#showVideo").click(function(){
          $('#sendFile').val('2');
          $("#file").attr('type','text');
          $("#file").attr('placeholder','e.g https://www.youtube.com/watch?v=J4Ty0Z6mVQo');
        });
    });
  </script>
  <!-- /.content-wrapper -->
@stop