@extends('layouts.frontend.header')
@extends('layouts.frontend.navbar')

<br>
<br>
<br>
<br>
    <!--SLIDER CROUSEL-->
    <div class="container-fluid" id="blog-images">
        <div class="container py-5">
            <div class="row">
                <div class="col-lg-8">
                    <div class="active-image">
                    <iframe width="100%" height="80%" src="https://www.youtube.com/embed/{{$blog_detail->thumbnail_image}}" frameborder="0"></iframe>
                </div>
                    <div class="col-lg-12 py-5 farmer">
                        <h1>{{ $blog_detail->title }}</h1>
                        {{--  <div style="float:left; margin-right: 20px;">
                            <img class="img-resonsive" src="img/farmer.jpg">
                        </div>  --}}
                        <div class="col-sm-12 pl-0">
                            <p>
                            <?php
                                $text =$blog_detail->description;
                                echo strip_tags($text);
                                ?>
                            </p>

                        </div>
                        <hr>
                        <div class="row justify-content-between share">
                            <div class="col-10">
                                <span>
                                    <span class="font-weight-bold"> share</span>
                                    <span class="share-link">
                                        <a href="https://www.facebook.com" class="share-f">
                                            <i class="fab fa-facebook-f"></i>
                                        </a>
                                        <a href="https://www.twitter.com" class="share-t">
                                            <i class="fab fa-twitter"></i>
                                        </a>
                                        <a href="https://www.googleplus.com" class="share-g">
                                            <i class="fab fa-google-plus-g"></i>
                                        </a>
                                        <a href="https://www.linkedin.com" class="share-l">
                                            <i class="fab fa-linkedin-in"></i>
                                        </a>
                                        <a href="https://www.pintrest.com" class="share-p">
                                            <i class="fab fa-pinterest-p"></i>
                                        </a>

                                    </span>
                            </div>
                            <div class="col-1 mr-3">
                                <div class="heart"></div>
                            </div>
                        </div>








                    </div>
                </div>


                <div class="col-lg-4 data-scroll bg-light">
                    <div class="col">
                        <div class="search-btn">
                            <div class="form-group">
                                <span class="fa fa-search form-control-icon"></span>
                                <input type="text" class="form-control" placeholder="Search">
                            </div>
                        </div>
                        <hr>

                        <!--Right image Next image-->
                        <h5>Recent Posts</h5>
                        @foreach ($blogs as $blog)
                        <a href="#" style="text-decoration:none">
                            <div class="row col" id="next-images">
                                @if($blog->file_type==1)
                                <div class="recent-image">
                                  <img class="rounded" src="{{ $blog->thumbnail_image }}" alt="person" width="100">
                                </div>
                               @else
                                <div class="recent-image">
                                 <iframe src="https://www.youtube.com/embed/{{ $blog->thumbnail_image}}" frameborder="0"></iframe></div>
                                @endif
                                <div class="col">
                                    <h6>{{ $blog->title }}</h6>
                                    <hr>
                                    <p>{{ $blog->created_at }}</p>
                                </div>
                            </div>
                        </a>
                        <br>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>



    <!--HEART SCRIPT-->
    <script>
        /* when a user clicks, toggle the 'is-animating' class */
        $(".heart").on('click touchstart', function () {
            $(this).toggleClass('is_animating');
            $(this).toggleClass('liked');
        });

        /*when the animation is over, remove the class*/
        $(".heart").on('animationend', function () {
            $(this).toggleClass('is_animating');
        });
    </script>
@extends('layouts.frontend.footer')
