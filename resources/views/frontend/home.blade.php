@extends('layouts.frontend.app')
@section('content')
    <!--HEADER-->
    <div class="container-fluid  bg-success text-center text-white">
        <div class="row">
            <div class="col">
                <div class="about py-4 ">
                    <h1 class="font-weight-bold">
                       Safdar Ali 
                    </h1>
                    <p class="lead">
                        Broadcast Journalist | Social Activist | Social Media Influencer
                    </p>
                </div>
            </div>
        </div>
    </div>


    <!--IMAGES SECTION-->
    <div class="container-fluid py-3 text-center">
        <div class="row">
            <div class="col-sm-6">
                <div class="main">
                    <div class="image1 d-inline">
                        <img src="{{asset('frontend/img/www.png')}}" alt="no-img" class="w-100 img-fluid">
                    </div>
                    <div class="disc py-4">
                        <h4>Broadcast Journalist</h4>
                        <p class="col broad">
                            He is currently working as a Broadcast Journalist/Presenter
                            at Hum News. Hum Network Ltd is one of Pakistan’s largest
                            media groups. Previously he was associated with Roze News and
                            hosted the only youth related talk show in Pakistan, 'Hum Jawan'
                            and the prime time current affairs program SachiBaat on Roze News.
                            He has also worked as a Program Host at PTV Home.
                        </p>
                    </div>
                </div>
            </div>

            <div class="col-sm-6">
                <div class="main">
                    <div class="image1 d-inline">
                        <img src="{{asset('frontend/img/www.png')}}" alt="no-img" class="img-responsive w-100">
                    </div>
                    <div class="disc py-4">
                        <h4>Motivational Speaker/Trainer​</h4>
                        <p class="col broad">
                            He has appeared as a keynote speaker on various TV
                            Channels across India and Pakistan including DunyaTV,
                            ARY News, Aaj TV and NDTV. He has appeared as a motivational
                            and guest speaker at a number of national conferences and
                            workshops including the prestigious TEDx forum. He is also a
                            debates coach and public speaking trainer and has conducted
                            over 50 training sessions.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!--SOCIAL-->
    <div class="container-fluid text-center text-white py-1" id="social">
        <div class="row">
            <div class="col-md-8">
                <div class="socail-activity py-4">
                    <i class="fas fa-id-card text-success"></i>
                    <h2 class="py-3">Social Media Activist</h2>
                    <p>
                        He is the Co-founder and the CEO of the largest social
                        media initiative of Pakistan, MeriAwazSuno. It is a non-profit
                        organization which aims to bring betterment and spread awareness
                        in the society through the most effective platform of the current
                        times, the social media and is fighting against injustice, poverty,
                        religious intolerance, child abuse and social evils of Pakistan.
                        It provides the youth of Pakistan an opportunity to amplify their
                        voices through short inspirational video messages.
                    </p>
                </div>
            </div>

            <!--Social Image-->
            <div class="col-md-4 py-4">
                <div class="social-image">
                    <img src="{{asset('frontend/img/awaz.jpg')}}" alt="awaz" width="100%" height="280">
                </div>
            </div>
        </div>
    </div>


    <!--Print Media-->
    <div class="container-fluid bg-light text-secondary py-3">
        <div class="row">
            <div class="col">
                <div class="print-image embed-responsive embed-responsive-16by9 h-100">
                    <img class="embed-responsive-item" src="{{asset('frontend/img/awaz.jpg')}}" alt="awaz2">
                </div>
            </div>

            <div class="col d-flex  text-center py-5">
                <div class="Print">
                    <i class="far fa-newspaper"></i>
                    <h3 class="print-h3 py-3">Print Media</h3>
                    <p class="print-p py-1">
                        He writes Urdu columns under the title “Zarb-e-Shahab”
                        and is working as International Correspondent of Daily
                        Pakistan. He also writes English Columns for Daily The
                        Patriot. He is known for highlighting social issues and
                        writes on topics which are considered a taboo in Pakistan.
                    </p>
                    <button class="btn btn-outline-secondary">Read Blog Post</button>
                </div>
            </div>
        </div>
    </div>


    <!--AWARD-->
    <div class="container-fluid text-center bg-success">
        <div class="row">
            <div class="col-lg-6">
                <div class="award">
                    <div id="star">
                        <i class="fas fa-star"></i>
                    </div>

                    <h2 class=" award-h">Public Speaking Champion</h2>
                    <p class="award-p">
                        He has won over 30 national public speaking championships and is the
                        recipient of the Best Speaker of Pakistan Award by the President of
                        Pakistan H.E. Mamnoon Hussain. He has also received the
                        Presidential Medal from Governor Punjab Ch. M. Sarwar.
                        <br>
                        He has represented and won laurels for Pakistan in a number of international
                        competitions, conferences and workshops. He is also the recipient of the
                        ‘Voice of Pakistan Excellence Award’
                    </p>
                </div>
            </div>
            <div class="col-lg-6 py-2">
                <img src="{{asset('frontend/img/award.jpg')}}" alt="award" width="100%">
            </div>
        </div>
    </div>


    <!--Education-->
    <div class="container-fluid" id="carier">
        <div class="container py-4">
            <div class="row">
                <div class="col-lg-6 py-3">
                    <div class="education">
                        <h3 class="text-center text-primary font-weight-bold">Education</h3>
                        <br>
                        <div class="row">
                            <div class="col row px-3">
                                <span id="za" class="fas fa-angle-right text-primary px-2"></span>
                                <div class="col row" id="train">
                                    BE Civil Engineering (2014-2018) NUST Islamabad, Pakistan
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col row">
                                <span id="za" class="fas fa-angle-right py-1 px-2 text-primary"></span>
                                <div class="col row" id="train">
                                    Bachelors in Mass Communication (2017-2019) Allama Iqbal Open University, Islamabad
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-6 py-3">
                    <div class="training">
                        <h3 class="text-center text-primary font-weight-bold">Trainings</h3>
                        <br>
                        <div class="row">
                            <div class="col row px-3">
                                <span id="za" class="fas fa-angle-right text-primary px-2"></span>
                                <div class="col row" id="train">
                                    BE Civil Engineering (2014-2018) NUST Islamabad, Pakistan
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col row py-1">
                                <span id="za" class="fas fa-angle-right py-1 px-2 text-primary"></span>
                                <div class="col row" id="train">
                                    Bachelors in Mass Communication (2017-2019) Allama Iqbal Open University, Islamabad
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col row px-3">
                                <span id="za" class="fas fa-angle-right text-primary px-2"></span>
                                <div class="col row" id="train">
                                    BE Civil Engineering (2014-2018) NUST Islamabad, Pakistan
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col row py-1">
                                <span id="za" class="fas fa-angle-right py-1 px-2 text-primary"></span>
                                <div class="col row" id="train">
                                    Bachelors in Mass Communication (2017-2019) Allama Iqbal Open University, Islamabad
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!--INFO-->
    <div class="container-fluid" id="news">
        <div class="container d-flex justify-content-center">
            <div class="col-sm-8 py-2">
                <video class="embed-responsive embed-responsive-16by9" controls id="myvideo">
                    <source src="{{asset('frontend/img/rain.mp4')}}" type="video/mp4">
                    <source src="rain.ogg" type="video/ogg">
                </video>
            </div>
        </div>
    </div>


    <!--RECENT BLOG-->
    <div class="section recent-blogs py-1">
        <div class="container-fluid">
            <div class="container-fluid">
                <h3><strong>Recent Blogs</strong></h3>
                <div class="row py-3">
                    <div class="col-md-4">
                        <div class="card-1">
                            <div class="card w-100">
                                <img class="card-img-top" src="{{asset('frontend/img/www.png')}}" alt="Card image cap">
                                <div class="card-body">
                                    <h6 class="card-title"><a href="#">How agriculture can eradicate rural poverty?</a></h6>
                                    <p class="card-text">How agriculture can eradicate rural poverty? The rural
                                        population,
                                        directly or indirectly is linked to agriculture for its …</small></p>

                                    <div class="row col">
                                        <div class="Blogs-image">
                                            <img class="rounded-circle" src="{{asset('frontend/img/person1.jpg')}}" alt="person" width="60">
                                        </div>
                                        <div class="col">
                                            <a href="#" class="blog-link">Makhdoom Shahab</a>
                                            <p class="blog-date">November 12, 2018</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-4">
                        <div class="card-1">
                            <div class="card w-100">
                                <img class="card-img-top" src="{{asset('frontend/img/www.png')}}" alt="Card image cap">
                                <div class="card-body">
                                    <h6 class="card-title"><a href="#">Problems of Agriculture in Pakistan</a></h6>
                                    <p class="card-text">Problems of Agriculture in Pakistan The magnanimity of the
                                        agricultural problems in Pakistan has undoubtedly crippled the …</p>

                                    <div class="row col">
                                        <div class="Blogs-image">
                                            <img class="rounded-circle" src="{{asset('frontend/img/person1.jpg')}}" alt="person" width="60">
                                        </div>
                                        <div class="col">
                                            <a href="#" class="blog-link">Makhdoom Shahab</a>
                                            <p class="blog-date">November 12, 2018</p>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card-1">
                            <div class="card w-100">
                                <img class="card-img-top" src="{{asset('frontend/img/www.png')}}" alt="Card image cap">
                                <div class="card-body">
                                    <h6 class="card-title"><a href="#">Report on Child Marriages in Pakistan</a></h6>
                                    <p class="card-text">Report on Child Marriages in Pakistan (English Subtitles)
                                        https://www.youtube.com/watch?v=Km2VLWXJPYQ&feature=youtu.be</p>
                                    <div class="row col">
                                        <div class="Blogs-image">
                                            <img class="rounded-circle" src="{{asset('frontend/img/person1.jpg')}}" alt="person" width="60">
                                        </div>
                                        <div class="col">
                                            <a href="#" class="blog-link">Makhdoom Shahab</a>
                                            <p class="blog-date">November 12, 2018</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <!--Youtube Video-->

    <div class="conatiner-fluid  bg-dark text-white py-5">
        <div class="container">
            <div class="row">
                <div class="col-4 py-4">
                    <h5 class="font-weight-bold">Like Me On Facebook</h5>
                    <div class="embed-responsive embed-responsive-1by1">
                        <iframe class="embed-responsive-item" src="https://www.youtube.com/watch?v=u6yMyrqoFf8"></iframe>
                    </div>
                </div>
                <div class="col-4 py-4">
                    <h5 class="font-weight-bold">Follow me On Twitter</h5>
                    <div class="embed-responsive embed-responsive-1by1">
                        <iframe class="embed-responsive-item" src="https://www.youtube.com/watch?v=u6yMyrqoFf8"></iframe>
                    </div>
                </div>
                <div class="col-4 py-4">
                    <h5 class="font-weight-bold">Follow me on Instagram</h5>
                    <div class="embed-responsive embed-responsive-1by1">
                        <iframe class="embed-responsive-item" src="https://www.youtube.com/watch?v=u6yMyrqoFf8"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>
  

@stop