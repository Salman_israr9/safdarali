@include('layouts.frontend.header')
@include('layouts.frontend.navbar')
    <!--SLIDER CROUSEL-->


    <!--RECENT BLOG-->
    <br>
    <br>
    <br>
    <br>
<div class="container">
<nav class="navbar navbar-expand-lg navbar-light bg-light">
@foreach($categories as $category)
  <a class="navbar-brand" style="cursor:pointer"
  href="{{ url('blog',$category->id) }}"
  >{{ $category->title }}</a>
@endforeach
</nav>
    </div>
    <div class="section blog-video py-4">
        <div class="container-fluid">
            <div class="container">
                <div class="row py-3">
                    @foreach($blogs as $blog)
                    @if($blog->file_type == 1)
                        <div class="col-md-4 py-3">
                            <div class="card-1">
                                <div class="card w-100">
                                    <img class="card-img-top" src="{{asset('blogs/'.$blog->thumbnail_image)}}" alt="Card image cap">
                                    <div class="card-body">
                                        <h6 class="card-title">
                                            <a href="index.html">
                                                <span class="fas fa-user"> Safdar Ali</span>
                                            </a>
                                            at <span class="far fa-clock"> December 16, 2018</span>
                                        </h6>

                                        <h5><a href="blog-img.html">{{$blog->title}}</a></h5>

                                        <!-- <small>Exclusive Report – World Science Day for Peace and Development</small> -->
                                    </div>
                                    <div class="blog-footer bg-light">
                                        <div class="row py-1 justify-content-between">
                                            <div class="col-1">
                                                <div class="heart"></div>
                                            </div>
                                            <span class="col-5 comment">
                                                <span class="fas fa-comment">
                                                    <a href="#" class=" text-primary">{{$blog->likes}}</a>
                                                    &nbsp;
                                                </span>
                                                <span class="fas fa-file-alt">

                                                    <a href="{{url('blog/detail',$blog->id)}}" class="text-primary">Read more</a>
                                                </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                     @else

                     <div class="col-md-4 py-3">
                            <div class="card-1">
                                <div class="card w-100">
                                <iframe src="https://www.youtube.com/embed/{{$blog->thumbnail_image}}" frameborder="0"></iframe>
                                    <div class="card-body">
                                        <h6 class="card-title">
                                            <a href="index.html">
                                                <span class="fas fa-user"> Safdar Ali</span>
                                            </a>
                                            at <span class="far fa-clock"> December 16, 2018</span>
                                        </h6>

                                        <h5><a href="blog-img.html">{{$blog->title}}</a></h5>

                                        <!-- <small>Exclusive Report – World Science Day for Peace and Development</small> -->
                                    </div>
                                    <div class="blog-footer bg-light">
                                        <div class="row py-1 justify-content-between">
                                            <div class="col-1">
                                                <div class="heart"></div>
                                            </div>
                                            <span class="col-5 comment">
                                                <span class="fas fa-comment">
                                                    <a href="#" class=" text-primary">{{$blog->likes}}</a>
                                                    &nbsp;
                                                </span>
                                                <span class="fas fa-file-alt">

                                                    <a href="{{url('blog/detail',$blog->id)}}" class="text-primary">Read more</a>
                                                </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    @endif
                    @endforeach
                </div>
            </div>
        </div>
    </div>


    <section>

                <nav aria-label="Page navigation">
                    <ul class="pagination justify-content-center">
                            {{ $blogs->links() }}
                    </ul>
                </nav>

    </section>



    <!--HEART SCRIPT-->
    <script>
        /* when a user clicks, toggle the 'is-animating' class */
        $(".heart").on('click touchstart', function () {
            $(this).toggleClass('is_animating');
            $(this).toggleClass('liked');
        });

        /*when the animation is over, remove the class*/
        $(".heart").on('animationend', function () {
            $(this).toggleClass('is_animating');
        });
        $("#description").append('')
    </script>
@include('layouts.frontend.footer')

