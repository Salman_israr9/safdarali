@include('../layouts/frontend/header')
@include('../layouts/frontend/navbar')
 <!--RECENT BLOG-->
 <div class="section blog-video py-5">
 
        <div class="container-fluid" style="margin-top:10%">
            <div class="container">
                <div class="row">
                    @foreach($videos as $video)
                    <div class="col-md-4"  style="height:100%">
                       <iframe src="https://www.youtube.com/embed/{{$video->video_id}}" frameborder="0"></iframe>
                      <!-- <iframe  src="" frameBorder="0"></iframe> -->
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@include('../layouts/frontend/footer')
