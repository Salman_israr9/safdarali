
    <!--SLIDER CROUSEL-->

    <div id="slider" class="carousel slide" data-ride="carousel">

        <!-- The slideshow -->
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img src="{{asset('frontend/img/mountains.jpg')}}" alt="Los Angeles">
            </div>
            <div class="carousel-item">
                <img src="{{asset('frontend/img/p.jpg')}}" alt="Chicago">
            </div>
            <div class="carousel-item">
                <img src="{{asset('profile/img/logo.png')}}" alt="New York">
            </div>
        </div>
    </div>
    
    <!--SLIDER CROUSEL-->

