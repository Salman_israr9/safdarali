  <!--FOOTER-->

  <section class="footer">
        <div class="container-fluid bg-dark">
            <div class="container">
                <div class="row">
                    <div class="footer text-white  py-4">
                        <p>© 2018 Makhdoom M. Shahab-ud-Din. All Rights Reserved Developed by <a href="www.zoner.doctrait.com">
                                Salman Israr </a></p>
                    </div>
                </div>
            </div>
        </div>
    </section>



    <!--NAVBAR ACTIVE JAVASCRIPT-->
    <script>
        // Add active class to the current button (highlight it)
        var header = document.getElementById("navbarCollapse");
        var btns = header.getElementsByClassName("nav-link");
        for (var i = 0; i < btns.length; i++) {
            btns[i].addEventListener("click", function () {
                var current = document.getElementsByClassName("active");
                current[0].className = current[0].className.replace(" active", "");
                this.className += " active";
            });
        }
    </script>
</body>

</html>