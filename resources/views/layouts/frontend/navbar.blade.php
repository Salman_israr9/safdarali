 <!--NAVABAR START-->
 <nav class="navbar navbar-expand-lg navbar-light fixed-top text-center" id="main-nav">
        <div class="container">
            <a href="index.html" class="navbar-brand">
                <img src="{{asset('profile/img/logo.png')}}" width="120" height="65" alt="no-img">
            </a>
            <button class="navbar-toggler" data-toggle="collapse" data-target="#navbarCollapse">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarCollapse">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a href="{{url('/')}}" class="nav-link">Home</a>
                    </li>
                    <li class="nav-item">
                        <a href="#about" class="nav-link">About</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{url('videos')}}" class="nav-link">Videos</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{url('blog')}}" class="nav-link">Blog</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{url('contact')}}" class="nav-link">Contact</a>
                    </li>
                    <li class="nav-item">
                        <i class="fas fa-search" id="btn"></i>
                    </li>
                    @if(Auth::guest())
                    <li class="nav-item">
                        
                        <a href="{{url('login')}}" class="nav-link">Login</a>
                    </li>
                    @else
                    <li class="nav-item">
                        <a href="{{url('home')}}" class="nav-link">Dashboard</a>
                    </li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>
    <!--NAVBAR END-->