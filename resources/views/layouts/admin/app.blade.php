<!-- header with files include -->
@include('./layouts/admin/header')
<!-- end header witch include befor body content -->
  <!-- Navbar -->
  @include('./layouts/admin/navbar')
  
  <!-- /.navbar -->
  @include('./layouts/admin/sidebar')

    <!-- sidebar -->

    <!-- end sidebar -->

  <!-- Content Wrapper. Contains page content -->
  <!-- middle content part -->
  @yield('content')
   <!-- end middle content part -->
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
   @include('././layouts/admin/controlSidebar')
  <!-- /.control-sidebar -->

  <!-- Main Footer -->
     @include('././layouts/admin/footerContent')
  <!--end  Main Footer -->


<!-- footer with all files -->

@include('././layouts/admin/footer')
<!-- end footer includes js files -->