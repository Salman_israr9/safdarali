<?php

use App\Http\Controllers\FrontendController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('frontend.home');
});

Route::get('/migrate',function()
{
    return Artisan::call('migrate:refresh');
});
Route::get('/seed',function()
{
    return Artisan::call('db:seed');
});
Route::get('/video',function()
{
    return view('frontend.upload_video');
});

Route::post('/uplaodVideo','VideoController@store');



Route::get('/profile', function () {
    return view('admin.profile');
});



/// front end routes
Route::get('blog','FrontendController@blogs');
Route::get('blog/{id}','FrontendController@blogs');
Route::get('blog/detail/{id}','FrontendController@blogDetail');

Route::get('videos','FrontendController@videos');

Route::get('contact','FrontendController@contact');



// Auth::routes();
Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout');
//Auth::routes();

// Authentication Routes...
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
// Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');



Route::get('/home', 'HomeController@index')->name('home');


/// admin routes
Route::middleware('auth')->prefix('admin')->group(function () {


    // category
    Route::get('category/create','AdminController@createCategory');
    Route::post('category/store','AdminController@storeCategory');
    Route::get('category/edit/{id}','AdminController@editCategory');
    Route::get('category/delete/{id}','AdminController@destroyCategory');
    Route::get('category/list','AdminController@listCategory');
    Route::post('category/update/{id}','AdminController@updateCategory');


    // video route which will upload from this site to youtube with detail
    Route::get('video/add','VideoController@create');
    Route::post('video/save','VideoController@store');
    Route::get('video/list','VideoController@index');

    /// blogs route

    Route::get('blog/list','AdminController@blogList');
    Route::get('blog/edit/{id}','AdminController@editBlog');
    Route::get('blog/delete/{id}','AdminController@deleteBlog');
    Route::get('blog/create','AdminController@createBlog');
    Route::post('blog/save','AdminController@saveBlog');
    Route::post('blog/update/{id}','AdminController@updateBlog');


    /// article routes
    Route::get('article/create','AdminController@createArticle');
    Route::get('article/edit/{id}','AdminController@editArticle');
    Route::get('article/delete/{id}','AdminController@deleteArticle');
    Route::post('article/save','AdminController@saveArticle');
    Route::post('article/update/{id)','AdminController@updateArticle');

    /// setting routes
    Route::get('general-setting','AdminController@showGeneralSettingForm');
    Route::get('account-setting','AdminController@showAccountSettingForm');
});
